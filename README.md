# README #

### What is this repository for? ###

* Quick summary
Source codes for simulation and analysis of percolation problem in dimension 3 to 7.  
Codes are related to publication: "From discrete to continuous percolation in dimensions 3 to 7" by Z. Koza and J. Pola.

* Version 0.1

### How do I get set up? ###

Source codes are using cmake build tool minimal version 2.8. 

* Dependencies
    + libgsl0-dev
    + boost
        - program options
        - random
    + openmp
    + gcc-4.8 (C++ 11)

Code was developed and tested on Ubuntu 14.04 LTS.

* Compilation
    1. Clone repository using ```git clone git@bitbucket.org:ismk_uwr/percolation.git```
    2. Go to ```percolation``` folder and create ```bin`` directory.
    3. Go to ```bin``` directory and call ```ccmake ../src``` the cmake configuration will open.
    4. Press ```c`` to create initial configuration of the project
    5. Fill the ```CMAKE_BUILD_TYPE``` with desired value ```Release``` or ```Debug```
    6. Press ```c``` to configure project and then ```g``` to generate Makefile.
    7. Type ```make``` to build the project
 
### Contribution guidelines ###
To contribute create a private fork. In your private fork make a new branch with proposed changes. After finishing create pull request to this repository.

### Who do I talk to? ###

* Repo owner or admin
    * Jakub Pola: jakub.pola2@uwr.edu.pl
    * Zbigniew Koza: zbigniew.koza@uwr.edu.pl


* Other community or team contact
    http://www.ift.uni.wroc.pl/~ismk/