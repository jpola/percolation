project(MyPercolation)
cmake_minimum_required(VERSION 2.8)

message("CMAKE_BUILD_TYPE is ${CMAKE_BUILD_TYPE}")

add_definitions(
    -std=c++11 # support for new cpp standard
)
# directory with external Find*.cmake files
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR}/cmake_modules)

# find Gnu scientific libraries
find_package(GSL REQUIRED)

#find BOOST components
find_package(Boost COMPONENTS
    program_options
    random
    #system
    #regex
    #filesystem
    REQUIRED)

#message(STATUS "Release Flags" ${CMAKE_CXX_FLAGS_RELEASE})
#message(STATUS "Debug Flags" ${CMAKE_CXX_FLAGS_DEBUG})


add_subdirectory(percolation)
#add_subdirectory(histogram)
#add_subdirectory(fit)
#add_subdirectory(percolation_wrapping)
