#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <vector>
#include <forward_list>
#include <map>
#include <cassert>
#include <cmath>
#include <sstream>
#include <algorithm>
#include <omp.h>
#include <boost/random/uniform_01.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>

//Create dict
#include <sys/stat.h>
#include <sys/types.h>

//parse cmd line params
#include "program_options.h"


#ifdef DEBUG_ZK
# define Debug_ZK(X) X
#else
# define Debug_ZK(X) 
#endif



template <typename T1, typename T2, int N>
T1 int_pow(T2 x)
{
    T1 res = x;
    for (int i = 1; i < N; i++)
        res *= x;
    return res;
}



template <typename RESULT, int DIM>
RESULT  make_index(int tab[], int L)
{
    RESULT result = tab[DIM-1];
    for (int i = DIM - 2; i >= 0; i--)
    {
        result *= L;
        result += tab[i];
    }
    return result;
}

// Structure representing single DIM dimensional obstacle in the system
template <int DIM, typename L_TYPE = int, typename V_TYPE = int>
struct Cube
{
    V_TYPE id;
    L_TYPE r[DIM];
    L_TYPE dist[DIM];

    void show(std::ostream & F, char c = '\n')
    {
        F << id << "\t[";
        for (int i = 0; i < DIM - 1; i++)
            F << r[i] << ",";
        F << r[DIM - 1] <<  "]\t (";
        for (int i = 0; i < DIM - 1; i++)
            F  << dist[i]<< ",";
        F << dist[DIM - 1] << ")" << c;
    }
};


// root_id elementu i zapisanego w tablicy uklad
// jednoczesnie scina posrednie odwolania do poziomo 1 owolania

/**
 * @brief get_root_id - gets the lowest(root) id(label) of the i-th element in cluster.
 *
 * @param i - index of the list of objects
 * @param system - search system
 * @return id of cluster to which the i-th object belongs to.
 */
template <int N, typename L_TYPE, typename  V_TYPE>
int get_root_id (int i, std::vector<Cube<N, L_TYPE, V_TYPE> > & uklad)
{
    Debug_ZK ( std::cout << __FUNCTION__ <<  " " << i << "->" << uklad[i].id <<  "\n" );

    int id = uklad[i].id;      // id of the direct ancestor
    if (id == uklad[id].id)
        return id;

    assert (id <= i);
    assert (id < (int) uklad.size() && id >= 0);
    assert (id > uklad[id].id);

    int root_id;
    if (uklad[id].id != id)
    {
        Debug_ZK(uklad[id].show(std::cout, ' '));
        Debug_ZK(std::cout << " -> " );
        root_id = get_root_id<N>(id, uklad);
        Debug_ZK(uklad[id].show(std::cout, '\n'));
    }
    else
        root_id = id;

    uklad[i].id = root_id;
    for (int j = 0; j < N; j++)
        uklad[i].dist[j]+= uklad[id].dist[j];

    return root_id;
}


/**
* @brief Combine clusters with i1, i2 ids and check whether they build a cycllic (wrapping) cluster
* @param i1 id of the first cluster
* @param i2 id of the second cluster
* @param uklad vector of obstacles in the system,
* @param K obstacle size,
* @param L system size,
* @return result answer the question: "Do the next1 and next2 clusters do NOT form a wrapping cluster"
* result == false: clusters wraps the space
* result == true:  clusters do not wrap.
*/
template <int DIM, typename L_TYPE, typename V_TYPE>
int combine_clusters (int i1, int i2, std::vector<Cube<DIM, L_TYPE, V_TYPE> > & uklad, const int K, const int  L)
{
    Debug_ZK( std::cout << __FUNCTION__ << " " << i1 << " with " << i2 << "\n");

    Debug_ZK(std::cout << "combining " << i1 << "->" << uklad[i1].id
             << " [" << uklad[i1].r[0] << "," << uklad[i1].r[1] << "] ("
                                       << uklad[i1].dist[0] << "," << uklad[i1].dist[1] << ") "
                                       << " [" << uklad[uklad[i1].id].r[0] << "," << uklad[uklad[i1].id].r[1] << "]"
                                      << " with "
                                      << i2 << "->" << uklad[i2].id
                                      << " [" << uklad[i2].r[0] << "," << uklad[i2].r[1] << "] ("
                                      << uklad[i2].dist[0] << "," << uklad[i2].dist[1]<< ")"
                                      << " [" << uklad[uklad[i2].id].r[0] << "," << uklad[uklad[i2].id].r[1] << "]"
                                      << "\n");

    auto pbc_dist = [L](int & n) {
        if (n > L/2)
            n -= L;
        else if
                (n < -L/2) n += L;
    };


    int result = 0;

    int next1 = uklad[i1].id;
    int next2 = uklad[i2].id;
    if (uklad[next2].id != next2)
        next2 = get_root_id(i2, uklad);

    assert (next1 <= i1);
    assert (next2 <= i2);
    assert (uklad[next1].id == next1);
    assert (uklad[next2].id == next2);
    for (int i = 0; i < DIM; i++)
    {
        assert (uklad[next1].dist[i] == 0);
        assert (uklad[next2].dist[i] == 0);
    }

    if (next1 == next2) // both  clusters have common ancestor
    {
        int dr[DIM];
        for(int j = 0; j < DIM; j++)
        {
            dr[j] =  uklad[i1].dist[j] - uklad[i2].dist[j];
            int tmp = std::abs(dr[j]) > K;
            tmp <<= j;
            result |= tmp;
        }

        Debug_ZK ( std::cout << "same ids; dx: " << dr[0]  << " dy: " << dr[1] << "\n" );

        Debug_ZK ( std::cout << "result: " << result << "\n" );

        return result; //klastry nie tworzą cyklu typu "wrapping" == true; tworzą == false
    }

    //next1 is a new root_id for i2
    assert (next1 < next2); // next1 jest nowym root-em dla i2

    int dr[DIM];
    for (int i = 0; i < DIM; i++)
    {
        dr[i] = uklad[i1].r[i] - uklad[i2].r[i];
        pbc_dist(dr[i]);
    }

    uklad[next2].id = next1;
    for (int j = 0; j < DIM; ++j)
    {
        uklad[next2].dist[j] = -uklad[i2].dist[j] + dr[j] + uklad[i1].dist[j];
    }

    for (int j = 0; j < DIM; j++)
    {
        Debug_ZK(std::cout << "**x[" << j << "] " << -uklad[i2].dist[j] << " " << dr[j] <<  " " << uklad[next1].dist[j] << "\n");
    }
    // **x 1 1 0
    // **y -1 0 0


    for(int j = 0; j < DIM; ++j)
        uklad[i2].dist[j] = dr[j] + uklad[i1].dist[j];

    uklad[i2].id     = next1;
    Debug_ZK ( std::cout << i2    << "->" << next1  << " dist_x: " << uklad[i2].dist[0]
            << " dist_y: " << uklad[i2].dist[1] << "\n" );
    Debug_ZK ( std::cout << i2    << "->" << next1  << " dx:     " << -uklad[i2].r[0] + uklad[next1].r[0]
            << " dy:     " << -uklad[i2].r[1] + uklad[next1].r[1] << "\n" );
    Debug_ZK ( std::cout << next2 << "->" << next1  << " dist_x: " << uklad[next2].dist[0]
            << " dist_y: " << uklad[next2].dist[1] << "\n" );
    Debug_ZK ( std::cout << next2 << "->" << next1  << " dx:     " << -uklad[next2].r[0] + uklad[next1].r[0]
            << " dy:     " << -uklad[next2].r[1] + uklad[next1].r[1] << "\n" );
    Debug_ZK ( std::cout << "result: " << result << "\n" );

#ifndef NDEBUG
    for (int i = 0; i < DIM; i++)
    {
        assert (std::abs(uklad[i2].dist[i] - (-uklad[i2].r[i] + uklad[next1].r[i])) % L == 0);
        assert (std::abs(uklad[next2].dist[i] - (-uklad[next2].r[i] + uklad[next1].r[i])) % L == 0);
    }
#endif

    return result;
}


/**
 * @brief Check wether obstacles of index my_index and index2 do overlap
 *
 * @oaran N - Dimension of the system / obstacle
 * @param K - obstacle size
 * @param L - system size
 * @param my_index - first index of the obstacle from uklad
 * @param index2 - second index of the obstacle from uklad to check if overlapping
 */
template <int N,typename L_TYPE, typename V_TYPE >
bool overlap (int K, int L, int my_index, int index2, std::vector<Cube<N, L_TYPE, V_TYPE>> const& uklad)
{
    assert (my_index < (int) uklad.size() && my_index >= 0);
    assert (index2 < (int) uklad.size()   && index2 >= 0);

    Cube<N, L_TYPE, V_TYPE> const& c1 = uklad[my_index];
    Cube<N, L_TYPE, V_TYPE> const& c2 = uklad[index2];

    // distance
    L_TYPE dr[N];

    int counter = 0;
    for (int i = 0; i < N; i++)
    {
        int d = std::abs (c1.r[i] - c2.r[i]);
        //check including cyclic boundary
        dr[i] = std::min (d, L - d);

        d = dr[i] - K;
        if (d > 0)
            return false;
        if (d == 0)
            counter++;
    }

    if (counter >= 2)
        return false;

    return true;
}


template <int DIM>
void fill_neighbour_1(int index, int side, std::vector<int> & neighbour, const int k_index[])
{
    neighbour.resize(2 * DIM + 1);
    neighbour[0] = index;
    int power = side;
    int subpower = 1;
    for (int i = 0; i < DIM; i++)
    {
        int result = index - subpower;
        if (k_index[i] == 0)
            result += power;
        assert (result >= 0 && (result < int_pow<int,int,DIM>(side)));
        neighbour[2*i+1] = result;

        result = index + subpower;
        if (k_index[i] == side - 1)
            result -= power;
        assert (result >= 0 && (result < int_pow<int,int,DIM>(side)));
        neighbour[2*i+2] = result;

        power *= side;
        subpower *= side;
    }
    //std::sort(neighbour.begin(), neighbour.end());
}


template <int DIM>
void fill_neighbour_N(int /*index*/, int side, std::vector<int> & neighbour, const int k_index[])
{
    neighbour.resize(int_pow<int, int, DIM>(3));

    // distance
    int dr[DIM];
    // init with -1
    std::fill(dr, dr + DIM, -1);
    // neighbour index
    int n_index[DIM];

    for (int j = 0; j < neighbour.size(); j++)
    {
        for (int i = 0; i < DIM; i++)
        {
            n_index[i] = k_index[i] + dr[i];
            if (n_index[i] < 0)
                n_index[i] = side - 1;
            else if (n_index[i] == side)
                n_index[i] = 0;
        }
        neighbour[j] = make_index<int, DIM>(n_index, side);
        assert (neighbour[j] >= 0 && (neighbour[j] < int_pow<int,int,DIM>(side)));

        for (int i = 0; i < DIM; i++)
        {
            dr[i]++;
            if (dr[i] < 2)
                break;
            dr[i] = -1;
        }
    }
}

inline void print_status_message(int DIM, int K, int L, int k_size, long long int V)
{
    if (omp_get_thread_num() == 0)
    {
        static bool first = true;
        if (first)
        {
            first = false;
            std::cout << "D = " << DIM << ", K = " << K << ", L = " << L
                      << ", k_size = " << k_size << ", V = " << V << std::endl;
        }
    }
}

template <int DIM, typename L_TYPE, typename V_TYPE = int>
void percolate (int K, int L, boost::random::mt19937 & rnd, std::vector<int> &results)
{

    //JPA TODO:: move it to parse command line?
    if (L % K != 0 && omp_get_thread_num() == 0)
    {
        std::cerr << "Error: L % K != 0\nAborting\n";
        exit(1);
    }

    std::fill(results.begin(), results.end(), 0);

    boost::random::uniform_int_distribution<> random (0, L - 1);
    std::vector<Cube<DIM, L_TYPE, V_TYPE>> uklad;

    std::vector<int> bins;
    std::vector<std::pair<int, int>> bins_table;

    int k_size = L/K;

    long long int V = int_pow<long long int, int, DIM>(L/K);

    print_status_message(DIM, K, L, k_size, V);

    bins.resize(V, -1);
    bins_table.reserve(V/10);

    int result = 0;
    int percolated = 0;
    int index;

    std::vector<int> overlapping;
    overlapping.reserve (1024);
    std::vector<int> neighbour;


    for ( ; ; )
    {
        const int my_index = uklad.size();
        Cube<DIM, L_TYPE, V_TYPE> cube;

        int k_index[DIM];
        for ( ; ; )
        {
            for (int i = 0; i < DIM; i++)
            {
                cube.r[i] = random(rnd);
                k_index[i] =  cube.r[i] / K;
            }
            index =  make_index<int, DIM>(k_index, L/K);
            assert (index < V && index >= 0);

            int v2 = bins[index];

            if (v2 < 0)
                break;

            bool ok = true;
            assert (0 <= v2 && v2 < (int)bins_table.size());
            while (v2 >= 0)
            {
                bool same = true;
                Cube<DIM, L_TYPE, V_TYPE> const& c = uklad[bins_table[v2].first];
                for (int i = 0; i < DIM; i++)
                {
                    same = same && c.r[i] == cube.r[i];
                    if (!same)
                        break;
                }
                ok = !same;
                if (!ok)
                    break;
                v2 = bins_table[v2].second;
                assert (v2 < 0 or (0 <= v2 && v2 < (int)bins_table.size()));
            }
            if (ok)
                break;
        }
        result++;

        for (int j = 0; j < DIM; j++)
            cube.dist[j] = 0;

        cube.id = uklad.size();
        uklad.push_back (cube);


        Debug_ZK(show(uklad, L, K));

        int gdzie = bins[index];
        bins_table.push_back(std::pair<int,int>(uklad.back().id , gdzie));
        bins[index] = bins_table.size() - 1;

        if (uklad.size() == 1)
            continue;

        if (K == 1)
            fill_neighbour_1<DIM>(index, L/K,  neighbour, k_index);
        else
            fill_neighbour_N<DIM>(index, L/K, neighbour, k_index);

        overlapping.clear();

        for (int i = 0; i < neighbour.size(); i++)
        {
            assert (neighbour[i] >= 0 && neighbour[i] < bins.size());

            int v2 = bins[neighbour[i]];

            while (v2 >= 0)
            {
                int index2 = bins_table[v2].first;
                v2 = bins_table[v2].second;
                assert (index2 >= 0);
                if (index2 == my_index)
                    continue;

                if (overlap<DIM, L_TYPE, V_TYPE>  (K, L, my_index, index2, uklad) )
                {
                    overlapping.push_back (index2);
                    Debug_ZK ( std::cout << "overlap-pushing " << index2 << "\n" );
                }
            }
        } // end neighbourhood loop

        if (overlapping.size() == 0)
            continue;

        overlapping.push_back (my_index);
        Debug_ZK ( std::cout << "overlap-pushing myindex " << my_index << "\n");

        int min_id = uklad.size(); // zaden hiperszescian nie ma tak wysokiego id
        int min_id_location = -1;

        for (int i = 0; i < (int) overlapping.size(); i++)
        {
            int n = get_root_id<DIM, L_TYPE, V_TYPE> (overlapping[i], uklad);

            if (n < min_id)
            {
                min_id = n;
                min_id_location = i;
            }
        }
        Debug_ZK ( std::cout << "min_id: " << min_id << " min_id_location: " << min_id_location << "\n" );
        assert (min_id >= 0 && min_id < (int) uklad.size() );


        int outcome = combine_clusters<DIM, L_TYPE, V_TYPE> (overlapping[min_id_location], overlapping.back(), uklad, K, L);

        for (int j = 0; j < DIM; j++)
        {
            int mask = 1 << j;
            if ((percolated & mask) == 0 &&  (outcome & mask) != 0)
            {
                results[j] = result;
                Debug_ZK ( std::cout << "percol in direction " << j << "\n" ) ;
            }
        }
        percolated |= outcome;
        Debug_ZK ( std::cout << "result, outcome, percolated: " <<  result << " " << outcome << " " << percolated << "\n" );


        for (int i = 0; i < (int) overlapping.size() - 1; i++)
        {
            if (i == min_id_location)
                continue;
            int outcome = combine_clusters(overlapping.back(), overlapping[i], uklad, K, L);
            for (int j = 0; j < DIM; j++)
            {
                int mask = 1 << j;
                if ((percolated & mask) == 0 &&  (outcome & mask) != 0)
                {
                    results[j] = result;
                    Debug_ZK ( std::cout << "percol in " << j << "\n" );
                }
            }
            percolated |= outcome;

            Debug_ZK ( std::cout << "result, outcome, percolated: " <<  result << " " << outcome << " " << percolated << "\n");
        }


        if (percolated == (1 << DIM) - 1)
            return;
    }

}


void createDirectory(const char* path)
{
    int status = mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);

    if (0 > status)
    {
        if(errno == EEXIST)
        {
            std::cerr << "\nDirectory " << path ;
            std::cerr << " already exists, files will be overwritten. \n" ;

        }
        if (errno != EEXIST)
        {
            std::cerr << "Can not create directory " << path;
            exit(2);
        }
    }
}

inline void setup (const int K, const int L, const int DIM,
                   const int nThreads,
                   std::ofstream& F)
{
    //create directory for storing results
    std::ostringstream dir;
    dir << "D" << DIM << "K" << K;
    createDirectory(dir.str().c_str());

    //setup output filename in dir
    std::ostringstream S;
    S << dir.str() <<"/percolD" << DIM << "K" << K << "L" << L << ".dat";
    std::cout << "Path : " << S.str() << std::endl;
    F.open(S.str().c_str(), std::ios::app);

    //setup number of threads for parallel computation
    omp_set_num_threads(nThreads);
}


int main (int argc, const char* argv[] )
{

    Debug_ZK ( std::cout << "DEBUG_ZK defined" << std::endl ) ;

    int DIM = 0;
    int K = 0;
    int L = 0;
    int max_n = 0;
    int nThreads = 0;

    bool parse = percolation::parseCommandLine(argc, argv,
                                               K, L, DIM, max_n, nThreads);

    if (!parse)
    {
        exit(1);
    }

    //setup output directory and file, set nThreads and open the output file.
    std::ofstream F;
    setup(K, L, DIM, nThreads, F);

    //prepare the seed and time
    int seed = 0;
    Debug_ZK(seed = 1234);
    time_t t0 = (seed == 0) ? time(0) : seed;
    std::cout << "seed[0]: " << t0 << std::endl;


#pragma omp parallel
    {
#pragma omp single

        std::cout << "num threads: " << omp_get_num_threads() << std::endl;
        boost::random::mt19937 rnd;
        rnd.seed(t0 + 1001*omp_get_thread_num());

#pragma omp for schedule (static, 1)
        for (int i = 0; i < max_n; i++)
        {
            std::vector<int> results(DIM);
            if (L <= 31)
            {
                switch (DIM)
                {
                case 2: percolate<2, char> (K, L, rnd, results); break;
                case 3: percolate<3, char> (K, L, rnd, results); break;
                case 4: percolate<4, char> (K, L, rnd, results); break;
                case 5: percolate<5, char> (K, L, rnd, results); break;
                case 6: percolate<6, char> (K, L, rnd, results); break;
                case 7: percolate<7, char> (K, L, rnd, results); break;
                default: throw std::string("unknown error at line ") +  std::to_string(__LINE__);
                }
            }
            else if (L < 4*1024)
            {
                switch (DIM)
                {
                case 2: percolate<2, short> (K, L, rnd, results); break;
                case 3: percolate<3, short> (K, L, rnd, results); break;
                case 4: percolate<4, short> (K, L, rnd, results); break;
                case 5: percolate<5, short> (K, L, rnd, results); break;
                case 6: percolate<6, short> (K, L, rnd, results); break;
                case 7: percolate<7, short> (K, L, rnd, results); break;
                default: throw std::string("unknown error at line ") +  std::to_string(__LINE__);
                }
            }
            else
            {
                switch (DIM)
                {
                case 2: percolate<2, int> (K, L, rnd, results); break;
                case 3: percolate<3, int> (K, L, rnd, results); break;
                case 4: percolate<4, int> (K, L, rnd, results); break;
                case 5: percolate<5, int> (K, L, rnd, results); break;
                case 6: percolate<6, int> (K, L, rnd, results); break;
                case 7: percolate<7, int> (K, L, rnd, results); break;
                default: throw std::string("unknown error at line ") +  std::to_string(__LINE__);
                }
            }

#pragma omp critical
            {
                for (int j = 0; j < DIM; j++)
                    F << results[j] << "\t";
                F << std::endl;
            }
        }
    }// end omp parallel

}



